const { names,data } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {}})
	})

	app.get('/people', (req, res) => {
		return res.send({
			people: names
		});
	})
	app.post('/currency', (req, res) => {
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter NAME';
			});
		}
		if(req.body.name !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - Name has to be a string';
			});
		}
		if(!req.body.name){
			return res.status(400).send({
				'error': 'Bad Request - Name should not be empty';
			});
		}
		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter EX';
			});
		}
		if(typeof(req.body.ex) !== 'object'){
			return res.status(400).send({
				'error': 'Bad Request - ex has to be a object';
			});
		}
		if(!req.body.ex){
			return res.status(400).send({
				'error': 'Bad Request - ex should not be empty';
			});
		}
		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter ALIAS';
			});
		}
		if(req.body.alias !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - Alias has to be a string';
			});
		}
		if(!req.body.alias){
			return res.status(400).send({
				'error': 'Bad Request - Alias should not be empty';
			});
		}
		const found = data.filter(el => el.alias == req.body.alias);
		if(!found && req.body.ex && req.body.name){
			return res.status(200).send({
				'pass': 'Good Request - Fields are all complete and no duplicate Alias found.';
			})
		}else if(found && req.body.ex && req.body.name){
			return res.status(400).send({
				'error': 'Bad Request - Fields are all complete but with duplicate Alias found.';
			});			
		}
	});
	app.post('/person', (req, res) => {
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter NAME'
			})
		}
		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - Name has to be a string'
			})
		}
		if(!req.body.hasOwnProperty('age')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter AGE'
			})
		}
		if(typeof req.body.age !== "number"){
			return res.status(400).send({
				'error': 'Bad Request - AGE has to be a number'
			})
		}
		if(typeof req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter alias'
			})
		}
	})

}