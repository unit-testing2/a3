function factorial(n){
	if(typeof n !== 'number') return undefined;
	if(n < 0) return undefined;
	if(n === 0) return 1;
	if(n === 1) return 1;
	return n * factorial(n - 1);
}

function oddEven(y){
	if(typeof n !== 'number') return undefined;
	if(y === 0)	return true
	return y % 2;
}

const div_check = (n) => {
	if(typeof n !== 'number') return undefined;
	if(n % 5 === 0) return true
	if(n % 7 === 0) return true
	return false
}

const names = {
	"Boba": {
		"name" : "Boba Fett",
		"age": 50
	},
	"Anakin":{
		"name": "Anakin Skywalker",
		"age": 65
	}
}

const data = {
	alias : 'riyadh',
	name  : 'Saudi Arabian Riyadh',
	ex    : {
			'peso' : 0.47,
			'usd'  : 0.092,
			'won'  : 10.93,
			'yuan' : 0.065
	}

}

module.exports = {
	factorial: factorial,
	oddEven: oddEven,
	div_check,
	names,
	data
}
