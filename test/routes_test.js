const { factorial, oddEven, div_check } = require('../src/util.js');
const chai = require('chai');
const expect = chai.expect;
// same as const {expect} = require("chai")

const http = require('chai-http');
chai.use(http);


describe('API Test for Currency', () => {
	it('[1] Test API Post /currency is running', () => {
		chai.request('http://localhost:5001').post('/currency')
		.end((err, res) => {
			expect(res).to.not.equal(undefined); 
		});
	});
	it('[2] Test API returns status 400 if name is missing',(done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 'riyadh',
			/*name  : 'Saudi Arabian Riyadh',*/
			ex    : {
					'peso' : 0.47,
					'usd'  : 0.092,
					'won'  : 10.93,
					'yuan' : 0.065
			}
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		});	
	});
	it('[3] Test API returns status 400 if name is not a string',(done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name : 123
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		});
	});
	it('[4] Test API returns status 400 if name is empty',(done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name : null
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		});
	});
	it('[5] Test API returns status 400 if ex is missing',(done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 'riyadh',
			/*name  : 'Saudi Arabian Riyadh',*/
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		});	
	});
	it('[6] Test API returns status 400 if ex is not an object',(done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			ex    : [{
					'peso' : 0.47,
					'usd'  : 0.092,
					'won'  : 10.93,
					'yuan' : 0.065
			}]
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		});	
	});
	it('[7] Test API returns status 400 if ex is empty',(done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			ex : {}
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		});	
	});
	it('[8] Test API returns status 400 if alias is missing',(done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			/*alias : 'riyadh',*/
			/*name  : 'Saudi Arabian Riyadh',*/
			ex    : {
					'peso' : 0.47,
					'usd'  : 0.092,
					'won'  : 10.93,
					'yuan' : 0.065
			}
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		});	
	});
	it('[9] Test API returns status 400 if alias is not a string',(done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 123,
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		});	
	});
	it('[10] Test API returns status 400 if alias is empty',(done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : null,
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		});	
	});
	it('[11] Test API returns status 400 if all fields are complete but with duplicate alias',(done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 'riyadh',
			name  : 'Saudi Arabian Riyadh',
			ex    : {
					'peso' : 0.47,
					'usd'  : 0.092,
					'won'  : 10.93,
					'yuan' : 0.065
			}
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		});	
	});
	it('[12] Test API returns status 200 all fields are complete and no duplicate alias',(done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 'Benny',
			name  : 'Saudi Arabian Riyadh',
			ex    : {
					'peso' : 0.47,
					'usd'  : 0.092,
					'won'  : 10.93,
					'yuan' : 0.065
			}
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		});	
	});
});


describe("API Test Suite", () => {
	it("Test API GET People are running", () => {
		chai.request('http://localhost:5001').get('/people')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})

	it("Test API GET people returns 200", (done) => {
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();
		})
	})

	it("Test API POST Person returns 400 if no person name", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Jason",
			age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})
	it("[1] Check if person endpoint is running", () => {
		chai.request('http://localhost:5001').post('/people')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	it("[2] Test API post person returns 400 if no ALIAS", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			age: 28,
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		})
	})
	it("[3] Test API post person returns 400 if no AGE", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "James"
		})
		.end((err, res) => {
			expect(res).to.not.equal(400);
			done();
		})
	})
})

describe('test_fun_factorials', () => {
	it('Test factorial -1 is undefined', () => {
		const product = factorial(-1);
		expect(product).to.equal(undefined);
	});
	it('Test factorial is not a non numerical', () => {
		const product = factorial('a')
		expect(product).to.equal(undefined);
	});
})

describe('test_num', () => {
	it('Test oddEven if input is a non numeric', () => {
		const input = oddEven("a");
		expect(input).to.equal(undefined); 
	});
});

describe('Check 5 and 7 Divisibility', () => {
	it('Test divCheck if input is a non numeric number', () => {
		const input = div_check("a");
		expect(input).to.equal(undefined);
	});
})