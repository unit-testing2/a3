const { factorial, oddEven, div_check } = require('../src/util.js');

const { expect, assert } = require('chai');

describe('test_fun_factorials', () => {
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	})

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1);
	});

	it('Test that is 0! is 1', () => {
		const product = factorial(0);
		assert.equal(product, 1);
	})

	it('Test that is 4! is 24', () => {
		const product = factorial(4);
		expect(product).to.equal(24);
	})

	it('Test that is 10! is 3628800', () => {
		const product = factorial(10);
		expect(product).to.equal(3628800);
	})

	
});

describe('test_num', () => {
	it('test_2_is_even', () => {
		const checkNum = oddEven(2);
		expect(checkNum).to.equal(0); // uses chainable language to construct assertions
	})

	it('test_5_is_odd', () => {
		const checkNum = oddEven(5);
		assert.equal(checkNum, 1); // uses assert-dot notation that node.js has
	})
})

describe('Check 5 and 7 Divisibility', () => {
	it('Asssert that 105 is divisible by 5', () => {
		const divisible = div_check(105);
		expect(divisible).to.equal(true)
	});

	it('Assert that 14 is divisible by 7', () => {
		const divisible = div_check(14);
		expect(divisible).to.equal(true);
	})

	it('Assert that 0 is divisible by both 5 and 7', () => {
		const divisible = div_check(0);
		expect(divisible).to.equal(true);
	})

	it('Assert that 22 is NOT divisible by both 5 and 7', () => {
		const divisible = div_check(22);
		expect(divisible).to.equal(false);
	})
})